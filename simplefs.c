#include <linux/module.h>    // included for all kernel modules
#include <linux/kernel.h>    // included for KERN_INFO
#include <linux/init.h>      // included for __init and __exit macros
#include <linux/fs.h>

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Amal Raj T");
MODULE_DESCRIPTION("simplefs");

static const struct super_operations simplefs_ops = {
    .statfs = simple_statfs,
    .drop_inode	= generic_drop_inode,
};

struct inode *simplefs_get_inode(struct super_block *sb, const struct inode *dir,
		int mode)
{
    struct inode *inode;
    inode = new_inode(sb);
    printk(KERN_ALERT "Started initializing inode");
    inode_init_owner(inode, dir, mode);
	inode->i_atime = inode->i_mtime = inode->i_ctime = current_time(inode);
	inode->i_ino = 1;
	inode->i_op = &simple_dir_inode_operations;
    inode->i_fop = &simple_dir_operations;

	inode->i_ino = get_next_ino();
	if (S_ISDIR(mode)) {
		inode->i_op = &simple_dir_inode_operations;
		inode->i_fop = &simple_dir_operations;
		inc_nlink(inode);
	}
    return inode;

}
static int simplefs_fill_super(struct super_block *sb, void *data, int silent)
{
    struct inode *inode;
    printk(KERN_ALERT "Started initializing superblock");
    sb->s_op = &simplefs_ops;
    inode = simplefs_get_inode(sb, NULL,
			S_IFDIR | S_IRWXU | S_IRGRP |
			S_IXGRP | S_IROTH | S_IXOTH);
    sb->s_root = d_make_root(inode);
    printk(KERN_ALERT "Done initializing superblock");
    return 0;    
}
static struct dentry *simplefs_mount(struct file_system_type *fs_type,
	int flags, const char *dev_name, void *data)
{
	return mount_nodev(fs_type, flags, data, simplefs_fill_super);
}

struct file_system_type simplefs_type = {
    .owner = THIS_MODULE,
    .name = "simplefs",
    .mount = simplefs_mount,
    .kill_sb	= kill_litter_super,
};

static int __init simplefs_init(void)
{
    printk(KERN_ALERT "Registering simplefs\n");
    register_filesystem(&simplefs_type);
    return 0;    // Non-zero return means that the module couldn't be loaded.
}

static void __exit simplefs_exit(void)
{
    printk(KERN_ALERT "De-registering simplefs.\n");
    unregister_filesystem(&simplefs_type);
}

module_init(simplefs_init);
module_exit(simplefs_exit);

