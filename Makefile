obj-m += simplefs.o

KERNEL_SRC=/lib/modules/$(shell uname -r)/build

all:
	${MAKE} -C ${KERNEL_SRC} M=$(shell pwd) modules

clean:
	${MAKE} -C ${KERNEL_SRC} M=$(shell pwd) clean

